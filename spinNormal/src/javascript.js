function createModal(inputCode, degree, price) {
    var spin = document.getElementById("spin");
    document.getElementById("keyframes").innerHTML = '@keyframes spin {0% {-webkit-transform: rotate('+degree[0]+'deg);-moz-transform: rotate('+degree[0]+'deg);-o-transform: rotate('+degree[0]+'deg);transform: rotate('+degree[0]+'deg);}90% {-webkit-transform: rotate('+degree[1]+'deg);-moz-transform: rotate('+degree[1]+'deg);-o-transform: rotate('+degree[1]+'deg);transform: rotate('+degree[1]+'deg);}95% {-webkit-transform: rotate('+degree[2]+'deg);-moz-transform: rotate('+degree[2]+'deg);-o-transform: rotate('+degree[2]+'deg);transform: rotate('+degree[2]+'deg)}100% {-webkit-transform: rotate('+degree[3]+'deg);-moz-transform: rotate('+degree[3]+'deg);-o-transform: rotate('+degree[3]+'deg);transform: rotate('+degree[3]+'deg);}}';
    document.getElementById("code").disabled = true;
    document.getElementById("spinBTN").disabled = true;
    spin.className = spin.className + "spinAround";
    setTimeout(function() {popUp(inputCode, price)}, 16500);
}
function popUp(inputCode, price) {
    var div = document.getElementById("reward");
    var title = document.createElement("H1");
    title.id = "congratz";
    title.appendChild(document.createTextNode("Congratulation!"));
    var content1 = document.createElement("P");
    content1.appendChild(document.createTextNode("You have won "));
    var content_strong_reward = document.createElement("P");
    var strong_reward = document.createElement("STRONG");
    strong_reward.id = "reward_content";
    strong_reward.appendChild(document.createTextNode(price));
    content_strong_reward.appendChild(strong_reward);
    var content2 = document.createElement("P");
    content2.appendChild(document.createTextNode("Please "));
    var strong_text = document.createElement("STRONG");
    strong_text.appendChild(document.createTextNode("Screenshot"));
    content2.appendChild(strong_text);
    content2.appendChild(document.createTextNode(" and contact our customer service to claim your reward."));
    var content3 = document.createElement("P");
    content3.appendChild(document.createTextNode("Code: " + inputCode));
    var d = new Date();
    var hour;
    var hour_int;
    var minute;
    if (d.getHours() > 12) {
        hour_int = d.getHours() - 12;
        day_part = "pm";
    }
    else {
        hour_int = d.getHours();
        day_part = "am";
    }
    if (hour_int.toString().length < 2)
        hour = "0" + hour_int;
    else
        hour = d.getHours();
    if (d.getMinutes().toString().length < 2)
        minute = "0" + d.getMinutes();
    else
        minute = d.getMinutes();
    var content4 = document.createElement("P");
    content4.appendChild(document.createTextNode("Time: " + hour + ":" + minute + " " + day_part));
    var content5 = document.createElement("P");
    content5.appendChild(document.createTextNode("Date: " + d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear()));
    div.appendChild(title);
    div.appendChild(content1);
    div.appendChild(content_strong_reward);
    div.appendChild(content3);
    div.appendChild(content4);
    div.appendChild(content5);
    div.appendChild(content2);
    document.getElementById("reward-container").style.display = "block";
}